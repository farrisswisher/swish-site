mod api;

#[tokio::main]
async fn main() {
    let routes = api::get_routes();
    warp::serve(routes).run(([0, 0, 0, 0], 3000)).await;
}
